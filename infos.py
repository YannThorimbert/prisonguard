hints = {
0:"Go to the door to escape a level.\nYou can swim into water, but lava kills you. Also, It is slower to run on sand.",
1:"Cops kill you when you touch them !\n",
2:"Press <LSHIFT> to throw a bomb. Only rock cannot be broken by explosions.\nBlue bombs slide on the ground until they hit something.",
3:"Press <LALT> to change projectile type. Green bombs can be thrown above obstacles.\nPress <LSHIFT> again to explode them.",
4:"Press <LALT> to change projectile type.\nRed bombs destroy all the blocks when sliding on the ground,\nexcept rock blocks.\nPress <LSHIFT> again to explode them.",
5:"Press <LALT> to change projectile type.\nGrass blocks can be thrown to help you evade obstacles.",
6:"Water, lava and sand are subject to gravity. Use bombs to manipulate the terrain\nor to kill guardians."
}




bombs = {
0:{"grass":0, "phys":0, "straight":0, "drill":0},
1:{"grass":0, "phys":0, "straight":0, "drill":0},
2:{"grass":0, "phys":0, "straight":10, "drill":0},
3:{"grass":0, "phys":10, "straight":10, "drill":0},
4:{"grass":0, "phys":10, "straight":10, "drill":10},
5:{"grass":10, "phys":10, "straight":10, "drill":10}
}