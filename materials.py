import thorpy, pygame
from imgprocessing import get_round
from worldobject import  get_sprite_frames

class Material:
    def __init__(self, name, fn, s, r, roundable=True):
        self.name = name
        self.fn = fn
        if isinstance(fn,str):
            img = thorpy.load_image(fn)
            img = pygame.transform.scale(img, (s,s))
        else:
            img = pygame.Surface((s,s))
            img.fill(fn)
        self.img_default = img
        self.imgs = {}
        def build_img(sides):
            if roundable:
                self.imgs[sides] = get_round(r, sides, img)
            else:
                self.imgs[sides] = img
        build_img("lt") #top left
        build_img("rt")
        build_img("bl")
        build_img("br")
        build_img("lrt")
        build_img("blt")
        build_img("blr") #bottom left right
        build_img("brt")
        build_img("blrt")
        self.roundable = roundable

    def set_alpha(self, a):
        self.img_default.set_alpha(a)
        for img in self.imgs.values():
            img.set_alpha(a)

    def get_img(self, sides):
        return self.imgs.get(sides, self.img_default)


class AnimatedMaterial:
    def __init__(self, name, fn, s):
        self.name = name
        self.fn = fn
        self.imgs = get_sprite_frames(fn)
        self.img_default = self.imgs[0]
        self.roundable = False
        self.frame = 0

    def update_frame(self):
        self.frame += 1
        self.frame %= len(self.imgs)

    def set_alpha(self, a):
        self.img_default.set_alpha(a)
        for img in self.imgs:
            img.set_alpha(a)

    def get_img(self, sides):
        return self.imgs[self.frame]
