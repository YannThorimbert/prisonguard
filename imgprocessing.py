import thorpy, pygame

HAS_SURFARRAY = True
try:
    from pygame import surfarray
    import numpy
except:
    HAS_SURFARRAY = False

def build_bkg_img(img, w, h):
    iw, ih = img.get_size()
    rw = w/iw
    rh = h/ih
    if h > ih:
##        if w > iw:
##            assert False
        return pygame.transform.smoothscale(img, (int(rh*iw),h))
    elif w > iw:
        while w > iw:
            new_img = pygame.Surface((2*iw, ih))
            new_img.blit(img, (0,0))
            new_img.blit(img, (iw,0))
            img = new_img
            iw *= 2
            rw = w/iw
    if rh > rw: #flat horitonal background, adapt according to height
        size = (int(rh*iw), h)
    else: #flat vertical background, adapt according to width
        size = (w, int(rw*ih))
    img = pygame.transform.smoothscale(img, size)
    iw = img.get_width()
    if iw < w:
        while w > iw:
            new_img = pygame.Surface((2*iw, ih))
            new_img.blit(img, (0,0))
            new_img.blit(img, (iw,0))
            img = new_img
            iw *= 2
            rw = w/iw
    if ih < h:
        img = pygame.transform.smoothscale(img, (int(iw*h/ih),h) )
    return img




def get_round(radius, sides, front):
    if not HAS_SURFARRAY:
        return front
    w,h = front.get_size()
    background = pygame.Surface((w,h))
    surface = front.copy().convert_alpha()
    newsurf = pygame.Surface((w,h), pygame.SRCALPHA,
                              depth=background.get_bitsize()).convert_alpha()
    newsurf.fill((0,0,0,0))
    inner = background.get_rect().inflate((-2*radius, -2*radius))
    n_a = surfarray.pixels_alpha(newsurf)
    n_rgb = surfarray.pixels3d(newsurf)
    b_b_c_rgb = surfarray.pixels3d(background)
    #Define ranges.
    topleft = (inner.left,inner.top,     0,inner.left,   0,inner.top)
    topright = (inner.right,inner.top,    inner.right,h,  0,inner.top)
    bottomleft = (inner.left,inner.bottom,  0,inner.left,   inner.bottom,h)
    bottomright = (inner.right-1,inner.bottom-1, inner.right,h,  inner.bottom,h)
    ranges = []
    if "t" in sides and "l" in sides:
        ranges.append(topleft)
    if "t" in sides and "r" in sides:
        ranges.append(topright)
    if "b" in sides and "l" in sides:
        ranges.append(bottomleft)
    if "b" in sides and "r" in sides:
        ranges.append(bottomright)
    r2 = radius**2
    for centerx,centery,xi,xf,yi,yf in ranges:
        for x in range(xi,xf):
            rx2 = (x-centerx)**2
            for y in range(yi,yf):
                if rx2 + (y-centery)**2 > r2:
                    n_a[x][y] = 255  #alpha background = 255
                    n_rgb[x][y] = b_b_c_rgb[x][y]
##    if "t" in sides:
##        for x in range(w):
##            for y in range(h//4):
##                n_a[x][y] = 127 #255 *  (1 - y / (h//4))  #alpha background = 255
##                n_rgb[x][y] = (220,220,220) #b_b_c_rgb[x][y]

    del n_a
    del n_rgb
    surface.unlock()
    newsurf.unlock()
    surface.blit(newsurf,(0,0))
    surface = surface.convert()
    surface.set_colorkey((0,0,0))
    return surface


def change_colors(level):
    pull_color = level.imgs_right[0].get_at((8,18))
    pant_color = level.imgs_right[0].get_at((10,28))
    pull_color_dark = level.imgs_right[0].get_at((7,22))
    pant_color_dark = level.imgs_right[0].get_at((15,26))
    orange = (231,76,37)
    orange_dark = (200, 60, 20)
    for imgs in (level.imgs_right, level.imgs_idle, level.imgs_left, level.imgs_fall, level.imgs_blood):
        for img in imgs:
            w,h = img.get_size()
            for x in range(w):
                for y in range(h):
                    coord = (x,y)
                    c = img.get_at(coord) #dirty, but doesnt work with surfarray
                    if c == pant_color or c == pull_color:
                        img.set_at(coord, orange)
                    if c == pant_color_dark or c == pull_color_dark:
                        img.set_at(coord, orange_dark)
