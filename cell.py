class Cell: #stores local parameters (destroyed, etc)

    def __init__(self, mat):
        self.mat = mat
        self.img_default = mat.img_default
        self.imgs = self.mat.imgs
        self.name = self.mat.name
        self.sides = None

    def to_data(self):
        return self.name
