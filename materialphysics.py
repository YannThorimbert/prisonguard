from cell import Cell
from constants import *

def update_cell_physics(level, coord):
    """Return the coord to draw with new material, and a boolean indicating
    if there is a splash"""
    cell = level.grid[coord]
    name = cell.name
    water = name == WATER
    sand = name == SAND
    magma = name == MAGMA
    water_or_magma = water or magma
    if sand or water_or_magma:
        x,y = coord
        down_xy = (x,y+1)
        down = level.get_cell_at(down_xy)
        if down.name == VOID:
            level.grid[coord] = Cell(level.base_mat)
            level.grid[down_xy] = Cell(cell.mat)
            return down_xy, False
        elif (sand or magma) and down.name == WATER:
            level.grid[coord] = Cell(level.base_mat)
            level.grid[down_xy] = Cell(cell.mat)
            return down_xy, False
        elif water_or_magma:
            left_xy = (x-1,y)
            left = level.get_cell_at(left_xy)
            right_xy = (x+1,y)
            right = level.get_cell_at(right_xy)
            left_void = left.name == VOID
            right_void = right.name == VOID
            if left_void and right_void: #splash
                level.grid[coord] = Cell(level.base_mat)
                return coord, True
            elif left_void: #flows to the left
                level.grid[coord] = Cell(level.base_mat)
                level.grid[left_xy] = Cell(cell.mat)
                return left_xy, False
            elif right_void: #flows to the right
                level.grid[coord] = Cell(level.base_mat)
                #does not need to be drawn now, as the loop will reach it later
                #but its okay
                level.grid[right_xy] = Cell(cell.mat)
                return right_xy, False

    return coord, False