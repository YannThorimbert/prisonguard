import os, thorpy, pygame, random
from thorpy import Monitor
from pygame.math import Vector2 as V2


from imgprocessing import build_bkg_img, change_colors
from worldobject import Char, AnimatedOnce, DialogObject
from worldobject import  get_sprite_frames
from ia import Helico, Guard, generate_guard, generate_helico
from materialphysics import update_cell_physics
from materials import Material, AnimatedMaterial
from cell import Cell
import infos
from constants import *

W,H = 1200, 700
S = 32
RADIUS = S//6
##RADIUS = 0
i_material = 0
i_bkg = 0
cursor_size = 1
pattern = "Normal"
run_ia = False
NEIGHS = (("b",(0,1)), ("l",(-1,0)), (ROCK,(1,0)), ("t",(0,-1))) #alphabetic !!!
FPS = 80
FRAME_SPEED1 = FPS//15
FRAME_SPEED2 = 2*FRAME_SPEED1
FRAME_SPEED3 = FRAME_SPEED1//2
MOD_PHYS = FPS//8
VEL_GUARD = 0.3
##MOD_PHYS = 1
SPRITES_FN = "./sprites/"
backgrounds = ["city.png",  "desert.png", "back.png", "background1.jpg",
                "landscape.png", "ocean.png"]
##backgrounds = ["ocean.png"]


level_number = 0
for fn in os.listdir("./"):
    if fn.startswith("level") and fn.endswith(".dat"):
        n = int(fn.split("level")[1].split(".dat")[0]) + 1
        if n > level_number:
            level_number = n
print("Level number", level_number)

mo = Monitor()

def sprite_path(fn):
    return os.path.join(SPRITES_FN,fn)

class Map:

    def __init__(self, nx, ny, s, bkg_fn, base_mat=VOID, border_mat=ROCK):
        self.mod_phys = MOD_PHYS
        self.nx = nx
        self.ny = ny
        self.W = nx*s
        self.H = ny*s
        self.s = s
        self.world_img = pygame.Surface((self.W, self.H))
        self.img_bkg = None
        self.set_bkg(bkg_fn)
        self.materials = None
        self.build_materials()
        self.outside_cell = Cell(self.outside)
        self.base_mat = self.materials[base_mat]
        self.border_mat = self.materials[border_mat]
        self.mat_names = list(self.materials.keys())
        self.grid = Grid(nx, ny, (s,s))
        for x in range(nx):
            for y in range(ny):
                self.grid[x,y] = Cell(self.border_mat)
        self.iteration = 0
        self.objects = [], []
        self.frame1 = 0
        self.frame2 = 0
        self.frame3 = 0
        self.screen = thorpy.get_screen()
        self.fps = 80
        self.g = V2(0, 5.)
        self.dt = 0.1
        self.editing = True
        self.imgs_right = get_sprite_frames(sprite_path("char_right.png"))
        self.imgs_left = get_sprite_frames(sprite_path("char_left.png"))
        self.imgs_idle = get_sprite_frames(sprite_path("char_idle.png"))
        self.imgs_fall = get_sprite_frames(sprite_path("char_falling.png"))
        self.cop_right = get_sprite_frames(sprite_path("cop1_right.png"))
        self.cop_left = get_sprite_frames(sprite_path("cop1_left.png"))
        imgs_door = get_sprite_frames(sprite_path("door.png"))
        self.door = AnimatedOnce(5, ny-2, self, imgs_door)
        self.door_open = False
        self.imgs_blood = get_sprite_frames(sprite_path("char_die.png"))
        self.helicopter_right = get_sprite_frames(sprite_path("helicopter_cop_right.png"), s=79)
        self.helicopter_left = []
        for img in self.helicopter_right:
            self.helicopter_left.append(pygame.transform.flip(img, True,False))
        change_colors(self)
        self.smokegen = thorpy.fx.get_fire_smokegen(n=100, color=(200,255,155),
                                grow=0.6, prob=0.9, black_increase_factor=2.)
        self.smokegen_cop = thorpy.fx.get_smokegen(n=50, color=(200,200,255), grow=0.6)
##        self.smokegen_sand = thorpy.fx.get_smokegen(n=50, color=(227,220,166), grow=0.8)
        self.bombs = get_sprite_frames(sprite_path("bombs.png"), s=12, resize_factor=(1.5,1.5))
        self.watersplash = get_sprite_frames(sprite_path("water_splash.png"),
                            resize_factor=(1.5,1.))
        self.imgs_explosion = get_sprite_frames(sprite_path("explosion.png"), s=96,
                            resize_factor=(1.,1.))
##        self.bullet = thorpy.load_image(sprite_path("bullet.png"))
        #add borders
        for x in range(nx):
            for y in range(ny):
                if x==0 or x==nx-1 or y == 0 or y == ny-1:
                    self.update_world((x,y), self.border_mat)
                else:
                    self.update_world((x,y), self.base_mat)
        self.char = Char(self.nx//2, self.ny//2, 1, self,
            self.imgs_right, self.imgs_left, self.imgs_idle, self.imgs_fall)
        self.char.smokegen = self.smokegen
        self.e_img_bomb = thorpy.Image(self.bombs[0])
        self.e_txt_bomb = thorpy.make_text("Projectile (10) : ", 20, (255,0,0))
        self.e_bomb = thorpy.make_group([self.e_txt_bomb, self.e_img_bomb])
        self.e_bomb.stick_to("screen", "top", "top")
        self.e_bomb.move((0,30))
        self.bombs[3] = self.materials[GRASS].imgs["blrt"]
        self.chicken = thorpy.load_image(sprite_path("chicken.png"))
        self.chicken.set_colorkey((0,0,0))
        img = [self.chicken]
        self.chicken = DialogObject(5,74,1,self,img, img, img, img)
        self.current_level = 0

    def set_bkg(self, fn):
        print("SET", fn)
        img = thorpy.load_image(fn)
        self.img_bkg = build_bkg_img(img, W+100, H+100)
        w,h = self.img_bkg.get_size()
        print(w,h)
        assert w >= W
        assert h >= H
        self.world_img.fill((0,0,0))

    def build_materials(self):
        grass = Material(GRASS, os.path.join("./sprites","grass.png"), self.s, RADIUS)
        sand = Material(SAND, os.path.join("./sprites","sand.jpg"), self.s, RADIUS)
##        dirt = Material("dirt", os.path.join("./sprites","dirt.jpg"), self.s, RADIUS)
        magma = AnimatedMaterial(MAGMA, os.path.join("./sprites","magma_anim.png"), self.s)
        water = AnimatedMaterial(WATER, os.path.join("./sprites","water_anim.png"), self.s)
##        water = Material(WATER, os.path.join("./sprites","water.png"), self.s, RADIUS)
        water.set_alpha(127)
        rock = Material(ROCK, os.path.join("./sprites","rock.jpg"), self.s, RADIUS)
        void = Material(VOID, (0,0,0), self.s, RADIUS, roundable=False)
        outside = Material(OUT, (0,0,0), self.s, RADIUS, roundable=False)
        self.materials = {GRASS:grass, SAND:sand, MAGMA:magma, #"dirt":dirt,
                          WATER:water, ROCK:rock, VOID:void}
        self.outside = outside


    def is_coord_valid(self, coord):
        x,y = coord
        return x >= 0 and x < self.nx and y >= 0 and y < self.ny


    def get_cell_at(self, coord):
        if self.is_coord_valid(coord):
            return self.grid[coord]
        else:
            return self.outside_cell


    def get_img_at(self, x, y):
        here = self.grid[x,y]
        if here.sides is None:
            #determine side (and hence image) of this brick
            here.sides = ""
            for side, delta in NEIGHS:
                xn, yn = x+delta[0], y+delta[1]
                n = (xn,yn)
                cell = self.get_cell_at(n)
                if cell.name != OUT:
                    if cell.name == VOID:
    ##                if cell.name != VOID:
                        here.sides += side
                else:
                    here.sides += side
        return here.mat.get_img(here.sides)
##        return here.imgs.get(sides, here.img_default)

    def build_world_img(self):
        update_phys = self.iteration%self.mod_phys == 0
        x0,y0 = self.grid.frame.topleft
        self.world_img.blit(self.img_bkg, (-x0-50,-y0-50))
        r = self.grid.get_rect_at_coord((0,0))
        self.water = []
        screen_rect = self.screen.get_rect()
        blits = []
        for y in range(self.ny-1,-1,-1): #reverse order because of gravity
            for x in range(self.nx):
                cell_name = self.grid[x,y].name
                splash = False
                if cell_name != VOID:
                    coord = (x,y)
                    if update_phys:
                        coord, splash = update_cell_physics(self, coord)
                    newx,newy = coord
                    is_water = cell_name == WATER
                    if splash and is_water:
                        watersplash = AnimatedOnce(newx-0.25, newy, self,
                                                    self.watersplash,
                                                    autokill=True)
                        self.objects.append(watersplash)
                    else:
                        xi = self.s*newx
                        yi = self.s*newy
                        r.x = xi + self.grid.frame.x
                        r.y = yi + self.grid.frame.y
                        if r.colliderect(screen_rect): #no unnecessary blits
                            img = self.get_img_at(newx,newy)
                            blits.append((img,(xi,yi)))
##                            self.world_img.blit(img, (xi,yi))
                    if is_water:
                        self.water.append(coord)
        self.world_img.blits(blits)


    def blit_img(self, coord):
        r = self.grid.get_rect_at_coord(coord)
##        r.left -= self.grid.frame.left
##        r.top -= self.grid.frame.top
        img = self.get_img_at(coord[0],coord[1])
        self.screen.blit(img, r)

    def update_world(self, coord, material):
        if self.is_coord_valid(coord):
            self.grid[coord] = Cell(material)
            for side, delta in NEIGHS:
                xn, yn = coord[0]+delta[0], coord[1]+delta[1]
                cell = self.get_cell_at((xn,yn))
                if cell.name != OUT and cell.name != VOID:
                    cell.sides = None

##            r = self.grid.get_rect_at_coord(coord)
##            r.left -= self.grid.frame.left
##            r.top -= self.grid.frame.top
##            img = self.get_img_at(coord[0],coord[1])
##            if material.name != VOID:
##                self.world_img.blit(img, r)


    def update_world_pattern(self, coord, mat_name):
        x0, y0 = coord
        for x,y in pattern_coords((x0,y0)):
            self.update_world((x,y), self.materials[mat_name])

    def save_world(self):
        f = open("level" + str(level_number) + ".dat", WATER)
        f.write(str(self.nx) + " " + str(self.ny) + "\n")
        f.write(str(i_bkg) + "\n")
        for x in range(self.nx):
            for y in range(self.ny):
                f.write(self.grid[x,y].to_data()+"\n")
        for obj in self.objects: #actually, bandits only
            if not(obj is self.char) and not(isinstance(obj,AnimatedOnce)) and not isinstance(obj,DialogObject):
                f.write(str(obj.q.x)+" "+str(obj.q.y)+" "+obj.name+"\n")
        f.write(str(self.door.q.x)+" "+str(self.door.q.y)+" door\n")
        f.write(str(self.char.q.x)+" "+str(self.char.q.y)+" char\n")
        f.close()

    def build_objects(self):
        new_objs = []
        helicos, guards = self.objects
        for coord in helicos:
            obj = Helico(coord[0], coord[1], 1, self)
            new_objs.append(obj)
        for coord in guards:
            obj = Guard(coord[0], coord[1], 1, self)
            new_objs.append(obj)
        new_objs.append(self.char)
        self.char.update_coord()
        self.objects = new_objs
        for o in self.objects:
            o.update_touch()

    def next_frame(self):
        mo.append("a")
        self.center_cam_on(self.char.q*self.s)
        mo.append("b")
        self.iteration += 1
        self.screen.fill((0,0,0))
        self.screen.blit(self.world_img, self.grid.frame)
##        for r in self.grid.iterrects():
##            pygame.draw.rect(self.screen, (255,255,255), r, 1)
        #draw objects #################################################
        self.control_end()
        self.door.update_coord()
        self.control_and_blit_objects()
        for coord in self.water:
            self.blit_img(coord)
        mo.append("c")
        if self.editing:
            self.draw_cursor_editing()
        self.e_bomb.blit()
        ##pygame.draw.rect(self.screen, (255,255,255), self.grid.frame, 1)
        pygame.display.flip()
##        app.pause()
        ########################################################################
        mo.append("d")
        if self.iteration%FRAME_SPEED1 == 0:
            self.frame1 += 1
        if self.iteration%FRAME_SPEED2 == 0:
            self.frame2 += 1
            self.materials[WATER].update_frame()
            self.materials[MAGMA].update_frame()
        if self.iteration%FRAME_SPEED3 == 0:
            self.frame3 += 1

        mo.append("e")
        self.build_world_img()
        mo.append("f")

        if self.iteration%self.fps == 0: #display stat every second
            mo.show(rnd=3)
            print()

    def control_end(self):
        global level_beeing_done
        if self.char.life <= 0. and self.char.blood.finish:
            for obj in self.objects:
                obj.draw()
            e = thorpy.make_text("WASTED", 70, (255,0,0))
            e.center()
            e2 = thorpy.make_text("Press a key to continue", 15, (255,0,0))
            e2.stick_to(e, "bottom", "top")
            e.blit()
            e2.blit()
            pygame.display.flip()
            app.pause()
            thorpy.functions.quit_menu_func()
        if not self.door_open:
            if self.char.is_touching(self.door) and not self.editing:
                self.door_open = True
                self.door.set_delta_i_now()
##                print("TOUCHE", self.door.q, self.char.q, self.char.is_touching(self.door))
        elif self.door.finish: #if animation finished
            self.objects.insert(0, self.door)
            for obj in self.objects:
                obj.draw()
            e = thorpy.make_text("LEVEL COMPLETED", 70, (255,255,0))
            e.center()
            e.blit()
            e2 = thorpy.make_text("Press a key to continue", 15, (255,0,0))
            e2.stick_to(e, "bottom", "top")
            e2.blit()
            pygame.display.flip()
            if self.current_level == 10:
                screen.fill((255,0,0))
                e = thorpy.make_text("YOU ARE FREE", 30, (255,255,0))
                e.center()
                e.blit()
            if level_number == self.current_level:
                self.current_level += 1
                level_beeing_done = self.current_level
            app.pause()
            thorpy.functions.quit_menu_func()

    def control_and_blit_objects(self):
        if self.door_open or self.char.life <= 0:
            if not(self.door is self.objects):
                self.objects.insert(0, self.door)
            for o in self.objects:
                o.draw()
            return
        self.door.draw_img(self.door.imgs_idle[0])
        ##############################
        blits = []
        for o in self.objects:
            o.choose_input()
            o.update_physics()
            o.draw()
            if not(isinstance(o, AnimatedOnce)):
                if self.get_cell_at(o.coord).name == MAGMA:
                    o.kill()
                else:
                    here = o.touch[HERE]
                    if here == MAGMA or here == SAND:
                        o.kill()
                if not(o is self.char):
                    if self.char.is_touching(o):
##                        o.kill()
                        self.char.kill()
                    else: #collisions
                        for o2 in self.objects:
                            if not(o2 is o):
##                                if o is self.chicken and o2 is self.char:
##                                    print("AAA", o.q, o2.q)
                                if not(isinstance(o2, AnimatedOnce)):
                                    if o.is_touching(o2):
                                        delta = o.q - o2.q
                                        delta.scale_to_length(10.)
                                        o2.f -= delta

    def get_mouse_coord(self):
        coord = self.grid.get_coord_at_pix(pygame.mouse.get_pos())
        return coord

    def draw_cursor_editing(self):
        pos = pygame.mouse.get_pos()
        coord = self.grid.get_coord_at_pix(pos)
        for coord2 in pattern_coords(coord):
            r = self.grid.get_rect_at_coord(coord2)
            screen.blit(self.materials[current_mat_name].img_default, r)
        rect = self.grid.get_rect_at_pix(pos)
        rect.w = cursor_size*S
        rect.h = cursor_size*S
        pygame.draw.rect(screen, (255,255,0), rect, 1)
##        pygame.display.flip()

    def center_cam_on(self, coord):
        frame = self.grid.frame
        dx = W//2 - coord[0] - frame.x
        dy = H//2 - coord[1] - frame.y
        self.grid.set_topleft((frame.x+dx, frame.y+dy))
        for o in self.objects:
            if o.smokegen:
                o.smokegen.translate_old_elements((dx,dy))

    def get_objects_at(self, coord):
        return [o for o in self.objects if o.coord == coord]


    def launch_end_animation(self):
        m = thorpy.Menu(fps=self.fps)
        e = thorpy.Element()
        i = 0
        thorpy.add_time_reaction(e, end_animation_frame)

    def refresh_bomb_info(self):
        bomb_name = self.char.bomb_types[self.char.ibomb]
        n = self.char.bombs[bomb_name]
        if n > 0:
            self.e_img_bomb.set_image(self.bombs[self.char.ibomb])
            self.e_txt_bomb.set_text("Projectile ("+str(n)+")")
            self.e_img_bomb.visible = True
            self.e_txt_bomb.visible = True
        else:
            self.e_img_bomb.visible = False
            self.e_txt_bomb.visible = False


class Grid(thorpy.PygameGrid):

    def get_coord_at_pix(self, pix):
        x = pix[0] - self.frame.left
        y = pix[1] - self.frame.top
        cx = int(x * self.nx / self.frame.width)
        cy = int(y * self.ny / self.frame.height)
        return (cx, cy)

def add_material_if_pressed(coord):
    state = pygame.mouse.get_pressed()
    mat_name = None
    if state[2]:#right click
        mat_name = VOID
    elif state[0]:
        mat_name = current_mat_name
    if mat_name:
        editor.update_world_pattern(coord, mat_name)
        editor.build_world_img()
        pygame.display.flip()

def mousemotion():
    coord = editor.grid.get_coord_at_pix(pygame.mouse.get_pos())
    add_material_if_pressed(coord)
    #
##    coord = editor.grid.get_coord_at_pix(pygame.mouse.get_pos())
##    cell = editor.get_cell_at(coord)
##    if cell:
##        print(coord, cell.name)

def click():
    coord = editor.grid.get_coord_at_pix(pygame.mouse.get_pos())
    objs = editor.get_objects_at(coord)
    if objs:
        state = pygame.mouse.get_pressed()
        if state[2]:#right click
            for o in objs:
                editor.objects.remove(o)
    add_material_if_pressed(coord)

def wheel(delta):
    global cursor_size
    cursor_size += delta
    if cursor_size > 10 or cursor_size < 0:
        cursor_size = 1

def next_bomb():
    editor.char.ibomb += 1
    editor.char.ibomb %= len(editor.char.bomb_types)
    editor.refresh_bomb_info()


def next_mat():
    global i_material, current_mat_name
    i_material += 1
    i_material %= len(editor.materials)
    current_mat_name = editor.mat_names[i_material]

def next_size():
    global cursor_size
    cursor_size *= 2
    if cursor_size > 10 or cursor_size < 0:
        cursor_size = 1

def next_pattern():
    global pattern
    patterns = ["Normal", "Horizontal", "Horizontal2", "Vertical", "Vertical2"]
    i = patterns.index(pattern)
    i += 1
    i %= len(patterns)
    pattern = patterns[i]
##    els = []
##    for p in patterns:
##        e = thorpy.make_text(p)
##        els.append(e)
##    choice = thorpy.launch_blocking_choices_str("Choose a pattern", patterns)
##    pattern = choice




def pattern_coords(coord):
    x0,y0 = coord
    if pattern == "Normal":
        for x in range(cursor_size):
            xpos = x0 + x
            for y in range(cursor_size):
                ypos = y0 + y
                yield xpos, ypos
    elif pattern == "Horizontal":
        for x in range(cursor_size*6):
            xpos = x0 + x
            for y in range(cursor_size):
                ypos = y0 + y
                yield xpos, ypos
    elif pattern == "Horizontal2":
        for x in range(cursor_size*12):
            xpos = x0 + x
            for y in range(cursor_size):
                ypos = y0 + y
                yield xpos, ypos
    elif pattern == "Vertical":
        for y in range(cursor_size*6):
            ypos = y0 + y
            for x in range(cursor_size):
                xpos = x0 + x
                yield xpos, ypos
    elif pattern == "Vertical2":
        for y in range(cursor_size*12):
            ypos = y0 + y
            for x in range(cursor_size):
                xpos = x0 + x
                yield xpos, ypos


def get_random_bkg_fn():
    choice = os.path.join("./backgrounds/",random.choice(backgrounds))
    print(choice)
    return choice



def load_world(fn, s):
    global i_bkg
    f = open(fn, ROCK)
    first_line = f.readline().rstrip()
    nx, ny = [int(x) for x in first_line.split(" ")]
    second_line = f.readline().rstrip()
    i_bkg = int(second_line)
    bkg_fn = os.path.join("./backgrounds/", backgrounds[i_bkg])
    world = Map(nx, ny, s, bkg_fn)
    for x in range(nx):
        for y in range(ny):
            name = f.readline().rstrip()[0]
            world.update_world((x,y), world.materials[name])
    coords_h = []
    coords_g = []
    while True:#non material infos
        data = f.readline()
        if not data:
            break
        data = data.split(" ")
        x = float(data[0])
        y = float(data[1])
        name = data[2].rstrip()
        if name == "helico":
            coords_h.append((x,y))
        elif name == "guard":
            coords_g.append((x,y))
        elif name == "char":
            world.char.q = V2(x,y)
        elif name == "door":
            world.door.q = V2(x,y) #door is alread an object
        world.door.update_coord()
        world.char.update_coord()
    world.objects = coords_h, coords_g
    f.close()
    return world


def add_bandit():
    coord = editor.grid.get_coord_at_pix(pygame.mouse.get_pos())
    bandit = generate_guard(coord, editor)
    editor.objects.append(bandit)

def add_helico():
    coord = editor.grid.get_coord_at_pix(pygame.mouse.get_pos())
    bandit = generate_helico(coord, editor)
    editor.objects.append(bandit)


def set_door():
    coord = editor.grid.get_coord_at_pix(pygame.mouse.get_pos())
    editor.door.q = V2(coord[0],coord[1])
    print("set door", coord)


def next_bkg():
    global i_bkg
    i_bkg += 1
    i_bkg %= len(backgrounds)
    fn = os.path.join("./backgrounds/",backgrounds[i_bkg])
    editor.set_bkg(fn)


def toggle_ia():
    global run_ia
    run_ia = not(run_ia)
    def nothing():
        pass
    for o in editor.objects:
        if isinstance(o, Helico) or isinstance(o, Guard):
            o.ia_activated = run_ia
            if isinstance(o, Guard):
                if run_ia:
                    o.velocity = VEL_GUARD
                else:
                    o.velocity = 0.

def activate_ia():
    for o in editor.objects:
        if isinstance(o, Helico) or isinstance(o, Guard):
            o.ia_activated = True
            if isinstance(o, Guard):
                o.velocity = VEL_GUARD

def toggle_physics():
    if editor.mod_phys < FPS*10000:
        editor.mod_phys = FPS*10000
    else:
        editor.mod_phys = MOD_PHYS
    print(editor.mod_phys)


def get_n_level(fn):
    return int(fn.split("level")[1].split(".dat")[0])




app = thorpy.Application((W,H), "Map")
screen = thorpy.get_screen()
levels_done = []
while True:
    saves = []
    for fn in os.listdir("./"):
        if fn.startswith("level") and fn.endswith(".dat"):
            n = get_n_level(fn)
            name = "Level " + str(n)
            saves.append(name)
    saves.sort(key=lambda x:int(x.split(" ")[1]))
##    saves.append("Nouveau")
    choice = thorpy.launch_blocking_choices_str("Level choice", saves,
                                title_fontsize=20, title_fontcolor=(255,0,0))
    if choice == "Nouveau":
        NX, NY = int(0.5*W//S), int(4.*H//S)
    ##        NX, NY = int(0.5*W//S), int(0.5*H//S)
        editor = Map(NX, NY, S, get_random_bkg_fn())
        editor.door.q = V2(2,2)
    else:
        choice = int(choice.split(" ")[1])
        level_number = choice
        level_beeing_done = level_number
        editor = load_world("level"+str(choice)+".dat", S)
    ##    toggle_physics()
    editor.editing = False
    editor.build_objects()
    editor.build_world_img()
    editor.grid.set_bottomleft((0,H))
    current_mat_name = editor.mat_names[0]
    e_bckgr = thorpy.Background()
    thorpy.add_time_reaction(e_bckgr, editor.next_frame)
    thorpy.add_keydown_reaction(e_bckgr, pygame.K_LALT, next_bomb)
    if editor.editing:
        thorpy.add_keydown_reaction(e_bckgr, pygame.K_LCTRL, next_mat)
    ##    thorpy.add_keydown_reaction(e_bckgr, pygame.K_LALT, next_size)
        thorpy.add_mousemotion_reaction(e_bckgr, mousemotion)
        thorpy.add_click_reaction(e_bckgr, click)
        thorpy.add_click_reaction(e_bckgr, wheel, {"button":4}, {"delta":-1})
        thorpy.add_click_reaction(e_bckgr, wheel, {"button":5}, {"delta":1})
        thorpy.add_keydown_reaction(e_bckgr, pygame.K_s, editor.save_world)
        thorpy.add_keydown_reaction(e_bckgr, pygame.K_c, next_pattern)
        thorpy.add_keydown_reaction(e_bckgr, pygame.K_h, add_helico)
        thorpy.add_keydown_reaction(e_bckgr, pygame.K_b, add_bandit)
        thorpy.add_keydown_reaction(e_bckgr, pygame.K_p, set_door)
        thorpy.add_keydown_reaction(e_bckgr, pygame.K_f, next_bkg)
        thorpy.add_keydown_reaction(e_bckgr, pygame.K_1, toggle_ia)
        thorpy.add_keydown_reaction(e_bckgr, pygame.K_2, toggle_physics)

    txt = infos.hints.get(level_number,"")
    if txt:
        editor.next_frame()
        thorpy.launch_blocking_alert("Hints", txt, font_size=20)
    activate_ia()

    bombs = infos.bombs.get(level_number,None)
    if bombs:
        editor.char.bombs = bombs
    editor.refresh_bomb_info()

    if level_number == 10:
        editor.objects.append(editor.chicken)
        editor.chicken.name = "Use this spicy chicken as the propelant of a rocket!\nKeep <SPACE> pressed to use it."

    editor.current_level = level_number
    m = thorpy.Menu(e_bckgr, fps=FPS)
    m.play(preblit=False)

app.quit()

#TODO:
    #cop die couleur
    #menu escape
    #+ jolis caracteres
    #liste des bandits est + simple et + efficace pour collisions
    #utiliser blitS pour objet (plutot que blit)
    #blitSSSS
    #enlever scrap bricks si pas utilise
    #README : dire avec quoi testé
    #tracker les cellules fluides dans une liste a part ? a priori fait juste gagner le test, car dans build_imgs...
    #physique de l'eau a la fin : vof avec blit partiel ?
    #parapente/deltaplane
    #nuage poussiere quand marche sur sable
