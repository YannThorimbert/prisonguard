import pygame, thorpy
from pygame.math import Vector2 as V2
from constants import *

def get_sprite_frames(fn, deltas=None, s=32, ckey=(255,255,255),
                        resize_factor=None):
    imgs = []
    sprites = pygame.image.load(fn)
    if s == "auto":
        s = sprites.get_width()
    n = sprites.get_width() // s
    h = sprites.get_height()
    if resize_factor:
        rx, ry = resize_factor
        s = int(rx*s)
        w,h = sprites.get_size()
        w = int(rx*w)
        h = int(ry*h)
        sprites = pygame.transform.scale(sprites, (w,h))
    if not deltas:
        deltas = [(0,0) for i in range(n)]
    x = 0
    print("Loading sprites", n, s, h, fn)
    for i in range(n):
        surf = pygame.Surface((s,h))
        surf.fill(ckey)
        surf.set_colorkey(ckey)
        dx, dy = deltas[i]
        surf.blit(sprites, (dx,dy), pygame.Rect(x,0,s,h))
        imgs.append(surf)
        x += s
    return imgs

class WorldObject:

    def __init__(self, x, y, m, world,
                    imgs_right, imgs_left, imgs_idle, imgs_falling):
        self.friction = 2.
        self.q = V2(x,y)
        self.w = world
        self.rect = pygame.Rect(x,y,imgs_right[0].get_width(),imgs_right[0].get_height())
        self.coord = None
        self.update_coord()
        self.v = V2()
        self.fx = 2.
##        self.fx = 10.
        self.fy = 3.
        self.m = m
##        self.touch = {LEFT:None, RIGHT:None, UP:None, DOWN:None, HERE:None}
        self.touch = [None, None, None, None, None]
        self.imgs_right = imgs_right
        self.imgs_left = imgs_left
        self.imgs_idle = imgs_idle
        self.imgs_falling = imgs_falling
        self.f = V2()
##        self.max_vy = 10.
        self.max_n_jumps = 15
        self.n_jumps = 0
        self.n_jump = 0
        self.color = (255,0,0)
        self.screen = thorpy.get_screen()
        #
        self.smokegen = None
        self.name = "object"

    def kill(self):
        pass

    def update_coord(self):
        self.rect.x = self.q.x*self.w.s + self.w.grid.frame.x
        self.rect.y = self.q.y*self.w.s + self.w.grid.frame.y
        self.coord = int(self.q[0]), int(self.q[1])

    def get_cell_at(self, dx, dy):
        x,y = self.get_coord()
        neigh = x+dx, y+dy
        return self.w.get_cell_at(neigh)


    def get_terminal_velocity(self):
        #friction = -2*v <==> mg = 2v <==> v = mg/2
        return self.m*self.w.g.y / 2.


    def touch_solid(self, x, y):
        coord = self.w.grid.get_coord_at_pix((x,y))
        cell = self.w.get_cell_at(coord)
        if cell.name != VOID:
            return cell.name
        else:
            return None


    def update_touch(self):
        D = int(self.rect.w*0.2)
        #left ##################################################################
        x = self.rect.left
        y = self.rect.centery
        self.touch[LEFT] = self.touch_solid(x,y)
        #right #################################################################
        x = self.rect.right
        self.touch[RIGHT] = self.touch_solid(x,y)
        #down ##################################################################
        x = self.rect.left + D
        y = self.rect.bottom
        bottomleft = self.touch_solid(x,y)
        x = self.rect.right - D
        bottomright = self.touch_solid(x,y)
        if bottomleft:
            self.touch[DOWN] = bottomleft
        elif bottomright:
            self.touch[DOWN] = bottomright
        else:
            self.touch[DOWN] = None
        #up ####################################################################
        x = self.rect.left + D
        y = self.rect.top
        topleft = self.touch_solid(x,y)
        x = self.rect.right - D
        topright = self.touch_solid(x,y)
        if topleft:
            self.touch[UP] = topleft
        elif topright:
            self.touch[UP] = topright
        else:
            self.touch[UP] = None
        #here ##################################################################
        self.touch[HERE] = self.touch_solid(self.rect.centerx, self.rect.centery)

    def update_physics(self):
        if abs(self.v.y) < 1e-1:
            self.v.y = 0
        self.update_coord()
        self.update_touch()
        self.f -= self.friction*self.v #friction
        #Free fall #############################################################
        down = self.touch[DOWN]
        if not down:
            self.f += self.m * self.w.g
        elif down == WATER or down == MAGMA:
            self.f += self.m * self.w.g / 3.
        elif down:
            if self.v.y > 0:
                self.v.y = 0
            self.n_jump = 0
        #Movement ##############################################################
        a = self.f / self.m
        self.v += a * self.w.dt
        #Stop by side walls ####################################################
        right = self.touch[RIGHT]
        if right == WATER:
            right = None
        if right and self.v.x > 0:
            self.v.x = 0
        left = self.touch[LEFT]
        if left == WATER:
            left = None
        if left and self.v.x < 0:
            self.v.x = 0
        up = self.touch[UP]
        if up == WATER:
            up = None
        if up and self.v.y < 0:
            self.v.y = 0
        self.q += self.v * self.w.dt
        self.f = V2()


    def draw(self):
        if self.smokegen:
            # process smoke
            self.smokegen.kill_old_elements()
            self.smokegen.update_physics(V2(0,0))
            self.smokegen.draw(self.screen)
        ##########
        if self.v.y > 0.95*self.get_terminal_velocity():
            i = self.w.frame1%len(self.imgs_falling)
            img = self.imgs_falling[i]
        else:
            abs_vx = abs(self.v.x)
            if abs_vx < 0.1:
                i = self.w.frame2%len(self.imgs_idle)
                img = self.imgs_idle[i]
            elif self.v.x > 0:
                i = self.w.frame1%len(self.imgs_right)
                img = self.imgs_right[i]
            elif self.v.x < 0:
                i = self.w.frame1%len(self.imgs_left)
                img = self.imgs_left[i]
            else:
                img = self.imgs_idle[0]
        self.draw_img(img)


    def draw_img(self, img):
        self.screen.blit(img, self.rect)
##        pygame.draw.rect(self.screen, self.color, self.rect, 1)

    def is_touching(self, other):
        if abs(self.rect.bottom - other.rect.bottom) < 5:
            if abs(self.rect.centerx - other.rect.centerx) < self.w.s//2:
                return True
        return False

    def can_jump(self):
        if self.n_jump == 0 and self.touch[DOWN]:
            return True
        elif self.n_jump < self.max_n_jumps:
            return True
        return False

    def choose_input(self):
        pass

class Char(WorldObject):

    def __init__(self, x, y, m, world,
                    imgs_right, imgs_left, imgs_idle, imgs_falling):
        WorldObject.__init__(self, x,y,m,world,
                                imgs_right, imgs_left, imgs_idle, imgs_falling)
        self.life = 1.
        self.blood = None
        self.bomb = None
        self.bombs = {GRASS:3, "phys":2, "straight":2, "drill":2}
        self.bomb_types = ["straight", "phys", "drill", GRASS,]
        self.ibomb = 0
        self.last_shot = -float("inf")
        self.name = "char"

    def kill(self):
        if not self.w.editing:
            self.life = 0.
            self.blood = generate_blood(self, self.w)
            self.w.objects.remove(self)

    def choose_input(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_UP]:
            if self.touch[HERE] == WATER:
                self.f.y -= self.fy
            elif self.can_jump():
                self.v.y = -self.fy
                self.n_jump += 1
##        else:
##            self.n_jump = 0
        if keys[pygame.K_LEFT]:
            self.f.x -= self.fx*self.get_factor_x()
        if keys[pygame.K_RIGHT]:
            self.f.x += self.fx*self.get_factor_x()
        if self.w.current_level == 10 and keys[pygame.K_SPACE]:
            self.v.y = -self.fy #flight
            self.smokegen.generate(V2(self.rect.midbottom))
        if keys[pygame.K_LSHIFT]:
            self.shot()

    def get_factor_x(self):
        if self.touch[HERE] == WATER:
            return 0.2
        elif self.touch[DOWN] == SAND:
            return 0.5
        return 1.


    def shot(self):
        type_ = self.bomb_types[self.ibomb]
        if self.bombs[type_] > 0:
            if not self.bomb and self.w.iteration - self.last_shot > self.w.fps//2:
                self.bombs[type_] -= 1
                self.w.refresh_bomb_info()
                if type_ == "phys":
                    o = PhysicalBomb(self, self.q.x, self.q.y, self.w.bombs[self.ibomb])
                    vx = 8.
                    o.v.y = -15.
                    if self.v.x >= 0:
                        o.v.x = vx
                    else:
                        o.v.x = -vx
                elif type_ == "straight":
                    o = StraightBomb(self, self.q.x, self.q.y, self.w.bombs[self.ibomb])
                    vx = 2.
                    o.v.y = 1.
                    if self.v.x >= 0:
                        o.v.x = vx
                    else:
                        o.v.x = -vx
                elif type_ == "drill":
                    o = DrillBomb(self, self.q.x, self.q.y, self.w.bombs[self.ibomb])
                    vx = 2.
                    o.v.y = 1.
                    if self.v.x >= 0:
                        o.v.x = vx
                    else:
                        o.v.x = -vx
                elif type_ == GRASS:
                    o = GrassBomb(self, self.q.x, self.q.y,
                                    self.w.materials[GRASS].imgs["blrt"])
                    o.friction = 1.
                    vx = 8.
                    o.v.y = -8.
                    if self.v.x >= 0:
                        o.v.x = vx
                    else:
                        o.v.x = -vx
                ###################
                self.w.objects.append(o)
                self.bomb = o
                self.last_shot = self.w.iteration
            elif self.bomb and self.w.iteration - self.last_shot > self.w.fps//2:
                self.bomb.tele_explode()
        elif self.w.iteration - self.last_shot > self.w.fps//2:
            if self.bomb:
                self.bomb.tele_explode()

    def is_touching(self, other):
        if other is self.bomb:
            return False
        else:
            return WorldObject.is_touching(self, other)



class PhysicalBomb(WorldObject):

    def __init__(self, launched_from, x, y, img):
        WorldObject.__init__(self, x, y, 1., launched_from.w,
                                [img], [img], [img], [img])
        self.launched_from = launched_from
        self.friction = 1.
        self.explosion = None
        self.it0 = launched_from.w.iteration
        self.name = "bomb"

    def kill(self):
        self.explode()

    def draw(self):
        if not self.explosion:
            self.draw_img(self.imgs_idle[0])

    def tele_explode(self):
        self.explode()

    def make_damage(self):
        x0,y0 = self.coord
        for x in range(-2,3):
            xi = x0 + x
            for y in range(-2,3):
                if abs(x) + abs(y) <= 3:
                    yi = y0+y
                    coord = (xi,yi)
                    cell = self.w.get_cell_at(coord)
                    if cell.name == GRASS or cell.name == SAND:
                        self.w.update_world(coord, self.w.base_mat)
                        generate_explosion(coord,self.w)
        for o in self.w.objects:
            if not isinstance(o,AnimatedOnce):
                x,y = o.q
                if abs(x-x0) + abs(y-y0) <= 3:
                    o.kill()

    def explode(self):
        if not self.explosion:
            self.explosion = generate_explosion(self.q, self.w)
            self.make_damage()

    def is_touching(self, other):
        if other is self.launched_from:
            return False
        if abs(self.rect.bottom - other.rect.bottom) < 5:
            if abs(self.rect.centerx - other.rect.centerx) < self.w.s//2:
                return True
        return False

    def update_physics(self):
        if self.explosion and self.explosion.finish:
            self.launched_from.bomb = None
            self.w.objects.remove(self)
        WorldObject.update_physics(self)

class GrassBomb(PhysicalBomb):

    def explode(self):
        self.launched_from.w.update_world(self.coord, self.launched_from.w.materials[GRASS])
        self.launched_from.bomb = None
        self.launched_from.w.objects.remove(self)

    def update_physics(self):
        PhysicalBomb.update_physics(self)
        if self.launched_from.w.iteration - self.it0 > 40 and self.v.length() < 0.05:
            self.explode()

class StraightBomb(PhysicalBomb):
    def tele_explode(self):
        pass

    def update_physics(self):
        if self.explosion and self.explosion.finish:
            self.launched_from.bomb = None
            self.w.objects.remove(self)
        self.update_coord()
        self.update_touch()
        #Free fall #############################################################
        down = self.touch[DOWN]
        if down:
            if self.v.y > 0:
                self.v.y = 0
        #Stop by side walls ####################################################
        right = self.touch[RIGHT]
        if right == WATER:
            right = None
        if right and self.v.x > 0:
            self.v.x = 0
            self.explode()
        left = self.touch[LEFT]
        if left == WATER:
            left = None
        if left and self.v.x < 0:
            self.v.x = 0
            self.explode()
        up = self.touch[UP]
        if up == WATER:
            up = None
        if up and self.v.y < 0:
            self.v.y = 0
            self.explode()
        self.q += self.v * self.w.dt

class DrillBomb(StraightBomb):

    def tele_explode(self):
        self.explode()

    def update_physics(self):
        if self.explosion and self.explosion.finish:
            self.launched_from.bomb = None
            self.w.objects.remove(self)
        self.update_coord()
        self.update_touch()
        #Free fall #############################################################
        down = self.touch[DOWN]
        if down:
            if self.v.y > 0:
                self.v.y = 0
        #Stop by side walls ####################################################
        right = self.touch[RIGHT]
        if right == ROCK and self.v.x > 0:
            self.v.x = 0
        elif right and self.v.x > 0:
            self.w.update_world((self.coord[0]+1,self.coord[1]), self.w.base_mat)
        left = self.touch[LEFT]
        if left == ROCK and self.v.x < 0:
            self.v.x = 0
        elif left and self.v.x < 0:
            self.w.update_world((self.coord[0],self.coord[1]), self.w.base_mat)
        ########################################################################
        up = self.touch[UP]
        if up == WATER:
            up = None
        if up and self.v.y < 0:
            self.v.y = 0
##            self.explode()
        self.q += self.v * self.w.dt

class AnimatedOnce(WorldObject):

    def __init__(self, x, y, world, imgs, autokill=False):
        WorldObject.__init__(self, x, y, 1., world, imgs, imgs, imgs, imgs)
        self.finish = False
        self.delta_i = 0
        self.autokill = autokill
        self.name = "animated once"

    def set_delta_i_now(self):
        self.delta_i = self.w.frame2%len(self.imgs_idle)

    def draw(self):
        if self.finish:
            img = self.imgs_idle[-1]
            self.draw_img(img)
        else:
            i = self.w.frame2%len(self.imgs_idle) - self.delta_i
            img = self.imgs_idle[i]
            self.draw_img(img)
            if img == self.imgs_idle[-1]:
                self.finish = True
                if self.autokill:
                    self.w.objects.remove(self)

class DialogObject(WorldObject):

    def is_touching(self, other):
        if other is self.w.char:
            if (self.q-other.q).length() < 1:
                thorpy.launch_blocking_alert("Important information", self.name,
                                                font_size=20)
                self.w.objects.remove(self)
                return True
        return False

def generate_blood(o, level):
    blood = AnimatedOnce(o.q.x, o.q.y, level, level.imgs_blood)
    level.objects.append(blood)
    blood.set_delta_i_now()
    return blood

def generate_explosion(coord, level):
    w = 96//(2*level.s)
    expl = AnimatedOnce(coord[0]-w, coord[1]-w, level, level.imgs_explosion, autokill=True)
    level.objects.append(expl)
    expl.set_delta_i_now()
    return expl

