import random
from pygame.math import Vector2 as V2
import worldobject
from constants import *


class Helico(worldobject.WorldObject):

    def __init__(self, x, y, m, world):
        imgs_r = world.helicopter_right
        imgs_l = world.helicopter_left
        worldobject.WorldObject.__init__(self, x,y,m,world, imgs_r, imgs_l, imgs_r, imgs_r)
        self.life = 1.
        self.blood = None
        self.ia_activated = False
##        self.smokegen = world.smokegen_cop
        self.name = "helico"
        self.velocity = 0.5

    def kill(self):
        self.life = 0.
##        self.blood = worldobject.generate_blood(self, self.w)
        self.blood = worldobject.generate_explosion(self.coord, self.w)
        self.w.objects.remove(self)

    def choose_input(self):
        if self.ia_activated:
            delta = self.w.char.q - self.q
            if delta.length() < 10:
                self.v = delta
                self.v.scale_to_length(self.velocity)
##                self.smokegen.generate(V2(self.rect.center))

    def update_physics(self):
        if abs(self.v.y) < 1e-1:
            self.v.y = 0
        self.update_coord()
        self.update_touch()
        if self.touch[HERE] == WATER:
            self.kill()
        self.f -= 1*self.v #friction
        #Free fall #############################################################
        down = self.touch[DOWN]
        if not down:
            self.f += self.m * self.w.g * 0.1
        elif down == WATER or down == MAGMA:
            self.f += self.m * self.w.g / 3.
        elif down:
            if self.v.y > 0:
                self.v.y = 0
        #Movement ##############################################################
        a = self.f / self.m
        self.v += a * self.w.dt
        #Stop by side walls ####################################################
        right = self.touch[RIGHT]
        if right == WATER:
            right = None
        if right and self.v.x > 0:
            self.v.x = 0
        left = self.touch[LEFT]
        if left == WATER:
            left = None
        if left and self.v.x < 0:
            self.v.x = 0
        up = self.touch[UP]
        if up == WATER:
            up = None
        if up and self.v.y < 0:
            self.v.y = 0
        self.q += self.v * self.w.dt
        self.f = V2()

    def draw(self):
        if self.smokegen:
            # process smoke
            self.smokegen.kill_old_elements()
            self.smokegen.update_physics(V2(0,0))
            self.smokegen.draw(self.screen)
        ##########
        if self.v.y > 0.95*self.get_terminal_velocity():
            i = self.w.frame3%len(self.imgs_falling)
            img = self.imgs_falling[i]
        else:
            abs_vx = abs(self.v.x)
            if abs_vx < 0.1:
                i = self.w.frame3%len(self.imgs_idle)
                img = self.imgs_idle[i]
            elif self.v.x > 0:
                i = self.w.frame3%len(self.imgs_right)
                img = self.imgs_right[i]
            elif self.v.x < 0:
                i = self.w.frame3%len(self.imgs_left)
                img = self.imgs_left[i]
            else:
                img = self.imgs_idle[0]
        self.draw_img(img)


class Guard(worldobject.WorldObject):
    def __init__(self, x, y, m, world):
        imgs_r = world.cop_right
        imgs_l = world.cop_left
        worldobject.WorldObject.__init__(self, x,y,m,world, imgs_r, imgs_l, imgs_r, imgs_r)
        self.life = 1.
        self.blood = None
        self.ia_activated = False
        self.velocity = 0.
        self.v.x = self.velocity
        if random.random() < 0.5:
            self.v.x *= -1
        self.name = "guard"

    def kill(self):
        self.life = 0.
        self.blood = worldobject.generate_blood(self, self.w)
##        self.blood = worldobject.generate_explosion(self.coord, self.w)
        self.w.objects.remove(self)

    def choose_input(self):
        pass


    def update_physics(self):
        if abs(self.v.y) < 1e-1:
            self.v.y = 0
        self.update_coord()
        self.update_touch()
        self.f -= self.friction*self.v #friction
        #Free fall #############################################################
        down = self.touch[DOWN]
        if not down:
            bounce_back = False
            diag_left = self.w.get_cell_at((self.coord[0]-1,self.coord[1]+1))
            if diag_left.name != VOID and diag_left.name != MAGMA:
                self.v.x = -1.
                bounce_back = True
            else:
                diag_right = self.w.get_cell_at((self.coord[0]+1,self.coord[1]+1))
                if diag_right.name != VOID and diag_right.name != MAGMA:
                    self.v.x = 1.
                    bounce_back = True
            if not bounce_back:
                self.f += self.m * self.w.g
        elif down == WATER or down == MAGMA:
            self.f += self.m * self.w.g / 3.
        elif down:
            if self.v.y > 0:
                self.v.y = 0
            self.n_jump = 0
        #Movement ##############################################################
        a = self.f / self.m
        self.v += a * self.w.dt
        #Stop by side walls ####################################################
        right = self.touch[RIGHT]
        if right == WATER:
            right = None
        if right and self.v.x > 0:
            self.v.x = -1
        left = self.touch[LEFT]
        if left == WATER:
            left = None
        if left and self.v.x < 0:
            self.v.x = 1
        up = self.touch[UP]
        if up == WATER:
            up = None
        if up and self.v.y < 0:
            self.v.y = 0
        if self.v.x < 0:
            self.v.x = -self.velocity
        else:
            self.v.x = self.velocity
        self.q += self.v * self.w.dt
        self.f = V2()

def generate_helico(coord, level):
    x,y = coord
    guard = Helico(x,y, 1, level)
    return guard

def generate_guard(coord, level):
    x,y = coord
    guard = Guard(x,y, 1, level)
    return guard